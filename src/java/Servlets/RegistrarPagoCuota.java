/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Controlador.GestorDB;
import Modelo.PagoCuota;
import Modelo.Socio;
import Modelo.TipoCuota;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author roberto
 */
public class RegistrarPagoCuota extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RegistrarPagoCuota</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RegistrarPagoCuota at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         if(request.getSession().getAttribute("usuario")==null){
           getServletConfig().getServletContext().getRequestDispatcher("/Login.jsp").forward(request, response);
       }
               
        //--------------------------------------------

        request.setAttribute("estado", 0);
        int idSocio=Integer.parseInt(request.getParameter("idSocio"));

        GestorDB gestor=new GestorDB();
        
        ArrayList lstPagoCuotaXSocio=gestor.lstPagoCuotaXSocio(idSocio);
        
        Socio socio=gestor.obtenerSocio(idSocio);
        TipoCuota tipoCuota =gestor.obtenerTipoCuota(socio.getIdTipoCuota());
        
        request.setAttribute("tipoCuota", tipoCuota);
        request.setAttribute("lstPagoCuotaXSocio", lstPagoCuotaXSocio);
        request.setAttribute("socio", socio);
        int[] lstMeses= {1,2,3,4,5,6,7,8,9,10,11,12};
        request.setAttribute("lstMeses", lstMeses );
        
        getServletConfig().getServletContext().getRequestDispatcher("/PagoCuota.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        GestorDB gestor=new GestorDB();
        
         int idSocio=Integer.parseInt(request.getParameter("txtIdSocio"));
         int mes=Integer.parseInt(request.getParameter("txtMes"));
         int anio=Integer.parseInt(request.getParameter("txtAnio"));
        
        int estado=(gestor.insertarPagoCuota(new PagoCuota(mes, anio, idSocio)))?1:2;
        request.setAttribute("estado", estado);
        
        //----------------------------------------------------------
        ArrayList lstPagoCuotaXSocio=gestor.lstPagoCuotaXSocio(idSocio);
        
        Socio socio=gestor.obtenerSocio(idSocio);
        TipoCuota tipoCuota =gestor.obtenerTipoCuota(socio.getIdTipoCuota());
        
        request.setAttribute("tipoCuota", tipoCuota);
        request.setAttribute("lstPagoCuotaXSocio", lstPagoCuotaXSocio);
        request.setAttribute("socio", socio);
        int[] lstMeses= {1,2,3,4,5,6,7,8,9,10,11,12};
        request.setAttribute("lstMeses", lstMeses );
        
         getServletConfig().getServletContext().getRequestDispatcher("/PagoCuota.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
