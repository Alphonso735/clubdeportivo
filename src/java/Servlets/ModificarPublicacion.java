/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Controlador.GestorDB;
import Modelo.Publicacion;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author roberto
 */
public class ModificarPublicacion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ModificarPublicacion</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ModificarPublicacion at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
      if(request.getSession().getAttribute("usuario")==null){
           getServletConfig().getServletContext().getRequestDispatcher("/Login.jsp").forward(request, response);
       }
        //-------------------------------------------------------
        
        GestorDB gestor=new GestorDB();
        Publicacion publicacion=gestor.obtenerPublicacion(Integer.parseInt(request.getParameter("idPublicacion")));
        
        request.setAttribute("publicacion", publicacion);
        request.setAttribute("estado", 0);
        
        getServletConfig().getServletContext().getRequestDispatcher("/UpdatePublicacion.jsp").forward(request, response);
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
      GestorDB gestor=new GestorDB();
      
      int idPublicacion=Integer.parseInt(request.getParameter("idPublicacion"));
      String texto=request.getParameter("txtPublicacion");
      int idActividad=Integer.parseInt(request.getParameter("idActividad"));
      boolean estado=Boolean.parseBoolean(request.getParameter("habilitada"));
      
      Publicacion p=new Publicacion(idPublicacion, texto, idActividad, estado);
      
      int estadoEjecucion=(gestor.modificarPublicacion(p))?1:2;
      request.setAttribute("estado",estadoEjecucion );
      
       Publicacion publicacion=gestor.obtenerPublicacion(Integer.parseInt(request.getParameter("idPublicacion")));
       request.setAttribute("publicacion", publicacion);
     // request.setAttribute("lstPublicaciones", gestor.lstPublicacionXActividad(idActividad, true));
     //    request.setAttribute("lstActividades", gestor.lstActividades());
      getServletConfig().getServletContext().getRequestDispatcher("/UpdatePublicacion.jsp").forward(request, response);
      
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
