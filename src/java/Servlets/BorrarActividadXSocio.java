/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Controlador.GestorDB;
import Modelo.Socio;
import Modelo.SocioXActividad;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author roberto
 */
public class BorrarActividadXSocio extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet BorrarActividadXSocio</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet BorrarActividadXSocio at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        GestorDB gestor=new GestorDB();
        
        int idSocio=Integer.parseInt(request.getParameter("idSocio"));
        int idActividad=Integer.parseInt(request.getParameter("idActividad"));
        
        gestor.desinscribirSocio(new SocioXActividad(idSocio, idActividad));
        
        Socio socio=gestor.obtenerSocio(idSocio);
        ArrayList lstActividadesXSocio=gestor.lstActividadXSocio(idSocio);
        ArrayList lstActividades=gestor.lstActividades();
        
        request.setAttribute("socio", socio);
        request.setAttribute("lstActividadesXSocio", lstActividadesXSocio);
        request.setAttribute("lstActividades", lstActividades);
        request.setAttribute("estado", 0);
        
        getServletConfig().getServletContext().getRequestDispatcher("/CargarActividadesASocio.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
