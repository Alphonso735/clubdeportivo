/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Controlador.GestorDB;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author roberto
 */
public class NoticiasController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet NoticiasController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet NoticiasController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
       response.setContentType("text/html;charset=UTF-8");
       //traigo la lista de actividades
       GestorDB gestor=new GestorDB();
       ArrayList lstActividades=gestor.lstActividades();
       
       //cargo la lista al request
       request.setAttribute("lstActividades", lstActividades);
       
       //--------------------------------
       ArrayList lstPublicaciones=new ArrayList();
       request.setAttribute("lstPublicaciones", lstPublicaciones);
       //---------------------------------
       
       //ahora mando el request y response al Noticias.jsp
       getServletConfig().getServletContext().getRequestDispatcher("/Noticias.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        GestorDB gestor=new GestorDB();
      
       //traigo la lista de actividades
       ArrayList lstActividades=gestor.lstActividades();
      //cargo la lista al request
       request.setAttribute("lstActividades", lstActividades);

       //---------------
        int idActividad=Integer.parseInt(request.getParameter("txtActividad"));
        ArrayList lstPublicaciones=gestor.lstPublicacionXActividad(idActividad,false);
        request.setAttribute("lstPublicaciones", lstPublicaciones);
    
      
        //ahora mando el request y response al Noticias.jsp
       getServletConfig().getServletContext().getRequestDispatcher("/Noticias.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
