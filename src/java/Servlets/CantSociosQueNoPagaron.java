/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Controlador.GestorDB;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author roberto
 */
public class CantSociosQueNoPagaron extends HttpServlet {

    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         if(request.getSession().getAttribute("usuario")==null){
           getServletConfig().getServletContext().getRequestDispatcher("/Login.jsp").forward(request, response);
       }
               
        //--------------------------------------------
       
        int[] lstMeses= {1,2,3,4,5,6,7,8,9,10,11,12};
        request.setAttribute("lstMeses", lstMeses );
        
        int resultado=-1;
        request.setAttribute("resultado", resultado);
        getServletConfig().getServletContext().getRequestDispatcher("/Reporte1.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
         int[] lstMeses= {1,2,3,4,5,6,7,8,9,10,11,12};
        request.setAttribute("lstMeses", lstMeses );
        
        GestorDB gestor=new GestorDB();
        
        int mes=Integer.parseInt(request.getParameter("txtMes"));
        int anio=Integer.parseInt(request.getParameter("txtAnio"));
        
        int resultado=gestor.cantSociosSinPagar(mes, anio);
        
        request.setAttribute("resultado", resultado);
        getServletConfig().getServletContext().getRequestDispatcher("/Reporte1.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
