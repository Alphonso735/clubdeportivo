/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Controlador.GestorDB;
import Modelo.TipoCuota;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author roberto
 */
public class ModificarTipoCuota extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ModificarTipoCuota</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ModificarTipoCuota at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         if(request.getSession().getAttribute("usuario")==null){
           getServletConfig().getServletContext().getRequestDispatcher("/Login.jsp").forward(request, response);
       }
               
        //--------------------------------------------
       
        GestorDB gestor=new GestorDB();
        
        int idTipoCuota=Integer.parseInt(request.getParameter("idTipoCuota"));
        TipoCuota cuotaAModificar=gestor.obtenerTipoCuota(idTipoCuota);
        
        request.setAttribute("tipoCuota", cuotaAModificar);
        request.setAttribute("estado", 0);
        
        getServletConfig().getServletContext().getRequestDispatcher("/UpdateTipoCuota.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       GestorDB gestor=new GestorDB();
       
       TipoCuota tipoCuotaAModificar=new TipoCuota(
               Integer.parseInt(request.getParameter("txtIdTipoCuota")),
               request.getParameter("txtNombre"),
               Float.parseFloat(request.getParameter("txtMonto"))
       );
       
       
       int estado=(gestor.modificarTipoCuota(tipoCuotaAModificar))?1:2;
       request.setAttribute("estado", estado);
       request.setAttribute("tipoCuota", tipoCuotaAModificar);
       
       getServletConfig().getServletContext().getRequestDispatcher("/UpdateTipoCuota.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
