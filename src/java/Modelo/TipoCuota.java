/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author roberto
 */
public class TipoCuota {
    private int id;
    private String nombre;
    private float monto;

    public TipoCuota(int id, String nombre, float monto) {
        this.id = id;
        this.nombre = nombre;
        this.monto = monto;
    }

    public TipoCuota(String nombre, float monto) {
        this.nombre = nombre;
        this.monto = monto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getMonto() {
        return monto;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }
    
    
}//finclase
