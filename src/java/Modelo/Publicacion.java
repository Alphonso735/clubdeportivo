/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author roberto
 */
public class Publicacion {
    private int id;
    private String texto;
    private int idActividad;
    private boolean habilitada;

    public Publicacion(int id, String texto, int idActividad, boolean habilitada) {
        this.id = id;
        this.texto = texto;
        this.idActividad = idActividad;
        this.habilitada = habilitada;
    }

    public Publicacion(String texto, int idActividad, boolean habilitada) {
        this.texto = texto;
        this.idActividad = idActividad;
        this.habilitada = habilitada;
        this.id=-1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public int getIdActividad() {
        return idActividad;
    }

    public void setIdActividad(int idActividad) {
        this.idActividad = idActividad;
    }

    public boolean isHabilitada() {
        return habilitada;
    }

    public void setHabilitada(boolean habilitada) {
        this.habilitada = habilitada;
    }
    
    
}//finclase
