/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Actividad;
import Modelo.PagoCuota;
import Modelo.Publicacion;
import Modelo.Socio;
import Modelo.SocioXActividad;
import Modelo.TipoCuota;
import Modelo.Usuario;
import Modelo.dotSocioCuota;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author roberto
 */
public class GestorDB {
    private Connection conexion;
    private String url;
    private String user;
    private String pass;

    public GestorDB() {
       /*
        this.url = "jdbc:sqlserver://172.16.140.13:1433;databaseName=TPtemaUno";
        this.user = "alumno1w1";
        this.pass = "alumno1w1";
        */
        this.url="jdbc:mariadb://localhost:3306/ClubDeportivo";
        this.user="roberto";
        this.pass="123";
    }
    
    
    //-----------------------------------
    private PreparedStatement obtenerConexion(String consulta){
        PreparedStatement resultado=null;
        try {
           // Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Class.forName("org.mariadb.jdbc.Driver");
            this.conexion=DriverManager.getConnection(this.url, this.user,this.pass);
            resultado=this.conexion.prepareStatement(consulta);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultado;
    }
    
    private void cerrarConexion(){
        try {
            this.conexion.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    //-*******************************************************************
     //insertar Actividad
    public boolean insertarActividad(Actividad a){
        boolean resultado=false;
        //----------------------
        String consulta="INSERT INTO Actividad (descripcion) VALUES(?);";
        PreparedStatement stmt=this.obtenerConexion(consulta);
        
        try {
            stmt.setString(1, a.getDescripcion());
            resultado=(stmt.executeUpdate()>0)?true:false;
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        //------------------------
         this.cerrarConexion();
        return resultado;
    }
    //modificar Actividad
    public boolean modificarActividad(int idActividad, String nuevaDescripcion){
        boolean resultado=false;
        String cadena="UPDATE Actividad SET descripcion=? WHERE id=?;";
        PreparedStatement stmt=this.obtenerConexion(cadena);
        try {
            stmt.setString(1, nuevaDescripcion);
            stmt.setInt(2, idActividad);
            resultado=(stmt.executeUpdate()>0)?true:false;
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
         this.cerrarConexion();
        return resultado;
    }
    private boolean existeActividad(int idActividad){
        boolean resultado=false;
        //---------------------------
        String consulta="SELECT COUNT(*) FROM Actividad WHERE id=?;";
        PreparedStatement stmt=this.obtenerConexion(consulta);
        try {
            stmt.setInt(1, idActividad);
            ResultSet datos=stmt.executeQuery();
            datos.next();
            resultado=(datos.getInt(1)>0)?true:resultado;
            datos.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cerrarConexion();
        //-------------------------
        return resultado;
    }
    private void desinscribirSocios(int idActividad){
        ArrayList<dotSocioCuota> lstSocios=this.lstSociosXActividad(idActividad);
        for(dotSocioCuota dot:lstSocios){
            this.desinscribirSocio(new SocioXActividad(dot.getId(), idActividad));
        }
    }
    private void borrarPublicacionesXActividad(int idActividad){
        ArrayList<Publicacion> lstPublicaciones=this.lstPublicacionXActividad(idActividad, true);
        for(Publicacion p:lstPublicaciones){
            this.borrarPublicacion(p.getId());
        }
    }
    //borrar una Actividad
    public boolean borrarActividad(int idActividad){
        boolean resultado=false;
        //-------------------------------
        if(this.existeActividad(idActividad))
        {
            this.desinscribirSocios(idActividad);
            this.borrarPublicacionesXActividad(idActividad);
            String consulta="DELETE FROM Actividad WHERE id=?;";
            PreparedStatement stmt=this.obtenerConexion(consulta);

            try {
                stmt.setInt(1, idActividad);
                resultado=(stmt.executeUpdate()>0)?true:false;
                stmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
         this.cerrarConexion();
        return resultado;
    }
    //listado de Actividades
    public ArrayList<Actividad> lstActividades(){
        ArrayList<Actividad> resultado=new ArrayList<Actividad>();
        //------------------------------------------
        String consulta="SELECT * FROM Actividad;";
        PreparedStatement stmt=this.obtenerConexion(consulta);
        
        try {
            ResultSet datos=stmt.executeQuery();
            while(datos.next()){
                resultado.add(new Actividad(datos.getInt(1),datos.getString(2)));
            }
            datos.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        //--------------------------------------------------
         this.cerrarConexion();
        return resultado;
    }
  
    //listado de actividades de un socio
    public ArrayList<Actividad> lstActividadXSocio(int idSocio){
        ArrayList<Actividad> resultado=new ArrayList<Actividad>();
        //--------------------------
        String consulta="SELECT a.id,a.descripcion FROM Actividad a JOIN SocioXActividad sa ON a.id=sa.idActividad WHERE sa.idSocio=?;";
        PreparedStatement stmt=this.obtenerConexion(consulta);
        try {
            stmt.setInt(1, idSocio);
            ResultSet datos=stmt.executeQuery();
            while(datos.next()){
                resultado.add(new Actividad(
                        datos.getInt(1),
                        datos.getString(2)
                ));
            }
            datos.close();
            stmt.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cerrarConexion();
        //--------------------------
        return resultado;
    }
   
    //***************************************************************
    //insertar publicacion
    public boolean insertarPublicacion(Publicacion p){
        boolean resultado=false;
        //------------------------
        String consulta="INSERT INTO Publicacion (texto,idActividad,habilitada) VALUES (?,?,?);";
        PreparedStatement stmt=this.obtenerConexion(consulta);
        
        try {
            stmt.setString(1, p.getTexto());
            stmt.setInt(2, p.getIdActividad());
            stmt.setBoolean(3, p.isHabilitada());
            
            resultado=(stmt.executeUpdate()>0)?true:false;
            
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //--------------------------
         this.cerrarConexion();
        return resultado;
    }
    //desabilitar una publicacion
    public boolean habilitarPublicacion(int idPublicacion, boolean estado){
        boolean resultado=false;
        //-----------------------
        String consulta="UPDATE Publicacion SET habilitada=? WHERE id=?;";
        PreparedStatement stmt=this.obtenerConexion(consulta);
        try {
            stmt.setBoolean(1, estado);
            stmt.setInt(2, idPublicacion);
            
            resultado=(stmt.executeUpdate()>0)?true:false;
            
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        //----------------------------------------------
         this.cerrarConexion();
        return resultado;
    }
    //borrar publicacion
    public boolean borrarPublicacion(int idPublicacion){
        boolean resultado=false;
        String consulta="DELETE FROM Publicacion WHERE id=?;";
        PreparedStatement stmt=this.obtenerConexion(consulta);
        try {
            stmt.setInt(1, idPublicacion);
            resultado=(stmt.executeUpdate()>0)?true:false;
            
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cerrarConexion();
        return resultado;
    }
    //modificar publicacion
    public boolean modificarPublicacion(Publicacion p){
        boolean resultado=false;
        String consulta="UPDATE Publicacion SET texto=?,idActividad=?,habilitada=? WHERE id=?;";
        PreparedStatement stmt=this.obtenerConexion(consulta);
        try {
            stmt.setString(1, p.getTexto());
            stmt.setInt(2, p.getIdActividad());
            stmt.setBoolean(3, p.isHabilitada());
            stmt.setInt(4, p.getId());
            resultado=(stmt.executeUpdate()>0)?true:resultado;
            
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cerrarConexion();
        return resultado;
    }
    //listado de publicaciones
    public ArrayList<Publicacion> lstPublicaciones(){
        ArrayList<Publicacion> resultado=new ArrayList<Publicacion>();
        //-------------------------------------------
        String consulta="SELECT * FROM Publicacion;";
        PreparedStatement stmt=this.obtenerConexion(consulta);
        
        try {
            ResultSet datos=stmt.executeQuery();
            while(datos.next()){
                resultado.add(new Publicacion(datos.getInt(1),datos.getString(2),datos.getInt(3),datos.getBoolean(4)));
            }
            datos.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cerrarConexion();
        //-------------------------------------
        return resultado;
    }
    //******************************************************
    //insertar socio
    public boolean insertarSocio(Socio s){
        boolean resultado=false;
        
        String consulta="INSERT INTO Socio (nombre,apellido,dni,direccion,telefono,mail,idTipoCuota) VALUES (?,?,?,?,?,?,?);";
        PreparedStatement stmt=this.obtenerConexion(consulta);
        
        try {
            stmt.setString(1, s.getNombre());
            stmt.setString(2, s.getApellido());
            stmt.setString(3, s.getDni());
            stmt.setString(4, s.getDireccion());
            stmt.setString(5, s.getTelefono());
            stmt.setString(6, s.getMail());
            stmt.setInt(7, s.getIdTipoCuota());
            
            resultado=(stmt.executeUpdate()>0)?true:resultado;
            
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return resultado;
    }
    //modificar un socio
    public boolean modificarSocio(Socio s){
        boolean resultado=false;
        //------------------------------
        String consulta="UPDATE Socio SET nombre=?,apellido=?,dni=?,direccion=?,telefono=?,mail=?,idTipoCuota=? WHERE id=?;";
        PreparedStatement stmt=this.obtenerConexion(consulta);
        
        try {
            stmt.setString(1, s.getNombre());
            stmt.setString(2, s.getApellido());
            stmt.setString(3, s.getDni());
            stmt.setString(4, s.getDireccion());
            stmt.setString(5, s.getTelefono());
            stmt.setString(6, s.getMail());
            stmt.setInt(7, s.getIdTipoCuota());
            stmt.setInt(8, s.getId());
            
            resultado=(stmt.executeUpdate()>0)?true:resultado;
            
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cerrarConexion();
        //------------------------------
        return resultado;
    }
    
    //borrar un socio
    public boolean borrarSocio(int idSocio){
        boolean resultado=false;
        //-------------------
        String consulta="DELETE FROM Socio WHERE id=?;";
        PreparedStatement stmt=this.obtenerConexion(consulta);
        
        try {
            stmt.setInt(1, idSocio);
            resultado=(stmt.executeUpdate()>0)?true:resultado;
            stmt.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cerrarConexion();
        //--------------------
        return resultado;
    }
    //listado de Socios
    public ArrayList<Socio> lstSocios(){
        ArrayList<Socio> resultado= new ArrayList<Socio>();
        //---------------------------
        String consulta="SELECT * FROM Socio;";
        PreparedStatement stmt=this.obtenerConexion(consulta);
        
        try {
            ResultSet datos=stmt.executeQuery();
            while(datos.next()){
                resultado.add(new Socio(datos.getInt(1),datos.getString(2),datos.getString(3),datos.getString(4),datos.getString(5),datos.getString(6),datos.getString(7),datos.getInt(8)));
            }
            datos.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cerrarConexion();
        //-----------------------
        return resultado;
        
    }
    //*************************************************************************
    //insertar tipo cuota
    public boolean insertarTipoCuota(TipoCuota tc){
        boolean resultado=false;
        //-----------------------
        String consulta="INSERT INTO TipoCuota (nombre,monto) VALUES (?,?);";
        PreparedStatement stmt=this.obtenerConexion(consulta);
        
        try {
            stmt.setString(1, tc.getNombre());
            stmt.setFloat(2, tc.getMonto());
            resultado=(stmt.executeUpdate()>0)?true:resultado;
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cerrarConexion();
        //-------------------
        return resultado;
    }
    //modificar tipo cuota
    public boolean modificarTipoCuota(TipoCuota tc){
        boolean resultado=false;
        //----------------------
        String consulta="UPDATE TipoCuota SET nombre=?,monto=? WHERE id=?;";
        PreparedStatement stmt=this.obtenerConexion(consulta);
        try {
            stmt.setString(1, tc.getNombre());
            stmt.setFloat(2, tc.getMonto());
            stmt.setInt(3, tc.getId());
            resultado=(stmt.executeUpdate()>0)?true:resultado;
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cerrarConexion();
        //----------------------
        return resultado;
    }
    //borrar un Tipo de cuota
    public boolean borrarTipoCuota(int idTipoCuota){
        boolean resultado=false;
        //-----------------
        String consulta="DELETE FROM TipoCuota WHERE id=?;";
        PreparedStatement stmt=this.obtenerConexion(consulta);
        try {
            stmt.setInt(1, idTipoCuota);
            resultado=(stmt.executeUpdate()>0)?true:resultado;
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cerrarConexion();
        //-----------------
        return resultado;
    }
    //listado de tipo cuota
    public ArrayList<TipoCuota> lstTipoCuota(){
        ArrayList<TipoCuota> resultado=new ArrayList<TipoCuota>();
        //---------------------------
        String consulta="SELECT * FROM TipoCuota;";
        PreparedStatement stmt=this.obtenerConexion(consulta);
        try {
            ResultSet datos=stmt.executeQuery();
            while(datos.next()){
                resultado.add(new TipoCuota(datos.getInt(1),datos.getString(2),datos.getFloat(3)));
            }
            datos.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cerrarConexion();
        //------------------------
        return resultado;
    }
    public TipoCuota obtenerTipoCuota(int idTipoCuota){
        TipoCuota resultado=null;
        //------------------------
        String consulta="SELECT * FROM TipoCuota WHERE id=?;";
        PreparedStatement stmt=this.obtenerConexion(consulta);
        try {
            stmt.setInt(1, idTipoCuota);
            ResultSet datos=stmt.executeQuery();
            datos.next();
            resultado=new TipoCuota(
                    datos.getInt(1),
                    datos.getString(2),
                    datos.getFloat(3)
            );
            datos.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cerrarConexion();
        //------------------------
        return resultado;
    }
    //****************************************************************
    //insertar un usuario
    public boolean insertarUsuario(Usuario u){
        boolean resultado=false;
        //--------------------
        String consulta="INSERT INTO Usuario (nombreUsuario, contrasenia) VALUES(?, ?);";
        PreparedStatement stmt=this.obtenerConexion(consulta);
        try {
            stmt.setString(1, u.getNombreUsuario());
            stmt.setString(2, u.getContrasenia());
            resultado=(stmt.executeUpdate()>0)?true:resultado;
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cerrarConexion();
        
        //--------------------
        return resultado;
    }
    //borrar un usuario
    public boolean borrarUsuario(int idUsuario){
        boolean resultado=false;
        //------------------------
        String cadena="DELETE FROM Usuario WHERE id=?";
        PreparedStatement stmt=this.obtenerConexion(cadena);
        try {
            stmt.setInt(1, idUsuario);
            resultado=(stmt.executeUpdate()>0)?true:resultado;
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cerrarConexion();
        //---------------------
        return resultado;
    }
    //modificar un usuario
    public boolean modificarUsuario(Usuario u){
        boolean resultado=false;
        //---------
        String cadena="UPDATE Usuario SET nombreUsuario=?,contrasenia=? WHERE id=?:";
        PreparedStatement stmt=this.obtenerConexion(cadena);
        try {
            stmt.setString(1, u.getNombreUsuario());
            stmt.setString(2, u.getContrasenia());
            stmt.setInt(1, u.getId());
            resultado=(stmt.executeUpdate()>0)?true:resultado;
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cerrarConexion();
        //---------------
        return resultado;
    }
   
    //consultar si existe un usuario
    public boolean existeUsuario(String user,String pass){
        boolean resultado=false;
        //-------------------------
        String cadena="SELECT COUNT(*) FROM Usuario WHERE nombreUsuario=? and contrasenia=?;";
        PreparedStatement stmt=this.obtenerConexion(cadena);
        try {
            stmt.setString(1, user);
            stmt.setString(2, pass);
            ResultSet datos=stmt.executeQuery();
            datos.next();
            resultado=(datos.getInt(1)>0)?true:resultado;
            stmt.close();
            datos.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cerrarConexion();
        //------------------------
        return resultado;
    }
    //**********************************************************
    //insertar el  Pago de una Cuota
    public boolean insertarPagoCuota(PagoCuota pc){
        boolean resultado=false;
        //------------------
        if(!existePC(pc))
        {
            String cadena="INSERT INTO PagoCuota (mes,anio,idSocio) VALUES (?,?,?);";
            PreparedStatement stmt=this.obtenerConexion(cadena);
            try {
                stmt.setInt(1, pc.getMes());
                stmt.setInt(2, pc.getAnio());
                stmt.setInt(3, pc.getIdSocio());
            
                resultado=(stmt.executeUpdate()>0)?true:resultado;
                stmt.close();
            
            } catch (SQLException ex) {
                Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        this.cerrarConexion();
        //------------------
        return resultado;
    }
    private boolean existePC(PagoCuota pc){
        boolean resultado=false;
        //------------------
        String consulta="SELECT COUNT(*) FROM PagoCuota WHERE mes=? and anio=? and idSocio=?;";
        PreparedStatement stmt=this.obtenerConexion(consulta);
        try {
            stmt.setInt(1, pc.getMes());
            stmt.setInt(2, pc.getAnio());
            stmt.setInt(3, pc.getIdSocio());
            
            ResultSet datos=stmt.executeQuery();
            datos.next();
            resultado=(datos.getInt(1)>0)?true:resultado;
            
            datos.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        //------------------
        return resultado;
    }
    public void borrarPagoCuota(PagoCuota pc){
        String consulta="DELETE FROM PagoCuota WHERE idSocio=? AND id=?;";
        PreparedStatement stmt=this.obtenerConexion(consulta);
        try {
            stmt.setInt(1, pc.getIdSocio());
            stmt.setInt(2, pc.getId());
            stmt.executeUpdate();
            this.cerrarConexion();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cerrarConexion();
        /*
        Version alternativa del borrado, poniendo en null el idSocio en PagoCuota
        
        String consulta="UPDATE PagoCuota SET idSocio=? WHERE idSocio=? AND id=?;";
        PreparedStatement stmt=this.obtenerConexion(consulta);
        try {
            stmt.setNull(1, pc.getId());
            stmt.setInt(2, pc.getIdSocio());
            stmt.setInt(3, pc.getId());
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cerrarConexion();
        */
    }
    
    //*******************************************
    //inscribir un socio a una actividad
    public boolean inscribirSocio(SocioXActividad sa){
        boolean resultado=false;
        //-----------------------
        String cadena="INSERT INTO SocioXActividad (idSocio,idActividad) VALUES (?,?);";
        PreparedStatement stmt=this.obtenerConexion(cadena);
        try {
            stmt.setInt(1, sa.getIdSocio());
            stmt.setInt(2, sa.getIdActividad());
            resultado=(stmt.executeUpdate()>0)?true:resultado;
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cerrarConexion();
        //-----------------------
        return resultado;
    }
    //desincribir un socio a una actividad
    public boolean desinscribirSocio(SocioXActividad sa){
        boolean resultado=false;
        ///---------------------------
        String cadena="DELETE FROM SocioXActividad WHERE idSocio=? and idActividad=?;";
        PreparedStatement stmt=this.obtenerConexion(cadena);
        try {
            stmt.setInt(1, sa.getIdSocio());
            stmt.setInt(2, sa.getIdActividad());
            resultado=(stmt.executeUpdate()>0)?true:resultado;
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cerrarConexion();
        ///---------------------------
        return resultado;
    }
    
    //*****************REPORTES************************************
    //cantidad de socios que no pagaron la cuota de algún mes/año que pueda seleccionarse
    public int cantSociosSinPagar(int mes, int anio){
        int resultado=0;
        //---------------
        String consulta="SELECT COUNT(*) from Socio as s WHERE s.id not in (select idSocio from PagoCuota where mes=? and anio=?);";
        PreparedStatement stmt=this.obtenerConexion(consulta);
        try {
            stmt.setInt(1, mes);
            stmt.setInt(2, anio);
            ResultSet datos=stmt.executeQuery();
            datos.next();
            resultado=datos.getInt(1);
            datos.close();
            stmt.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cerrarConexion();
        //---------------
        return resultado;
    }
    
    public float montoTotalPagadoAnual(int anio){
        float resultado=0f;
        //------------------------
        String consulta="SELECT SUM(tc.monto) FROM PagoCuota pc JOIN Socio s ON pc.idSocio=s.id JOIN TipoCuota tc ON s.idTipoCuota=tc.id WHERE pc.anio=?;";
        PreparedStatement stmt=this.obtenerConexion(consulta);
        try {
            stmt.setInt(1, anio);
            ResultSet datos=stmt.executeQuery();
            datos.next();
            resultado=datos.getFloat(1);
            datos.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cerrarConexion();
        //------------------------
        return resultado;
    }
    public ArrayList<dotSocioCuota> lstSociosXActividad(int idActividad){
        ArrayList<dotSocioCuota> resultado=new ArrayList<dotSocioCuota>();
        //--------------------------
        String consulta="SELECT s.id,s.nombre,s.apellido,s.dni,s.direccion,s.telefono,s.mail,tc.nombre,tc.monto \n" +
"		FROM Socio AS s JOIN TipoCuota AS tc ON s.idTipoCuota=tc.id\n" +
"		WHERE s.id in (SELECT idSocio FROM SocioXActividad WHERE idActividad=?);";
        PreparedStatement stmt=this.obtenerConexion(consulta);
        try {
            stmt.setInt(1, idActividad);
            ResultSet datos=stmt.executeQuery();
            while(datos.next()){
                resultado.add(new dotSocioCuota(datos.getInt(1), datos.getString(2), datos.getString(3), datos.getString(4), datos.getString(5), datos.getString(6), datos.getString(7), datos.getString(8), datos.getFloat(9)));
            }
            datos.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cerrarConexion();
        //--------------------------
        return resultado;
    }
    public ArrayList<PagoCuota> lstPagoCuotaXSocio(int idSocio){
        
        ArrayList<PagoCuota> resultado=new ArrayList<PagoCuota>();
        //-------------------------------
        String consulta="SELECT * FROM PagoCuota WHERE idSocio=? ORDER by anio,mes;";
        PreparedStatement stmt=this.obtenerConexion(consulta);
        try {
            stmt.setInt(1, idSocio);
            ResultSet datos=stmt.executeQuery();
            while(datos.next()){
                resultado.add(new PagoCuota(datos.getInt(1), datos.getInt(2), datos.getInt(3), datos.getInt(4)));
            }
            datos.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cerrarConexion();
        //-------------------------------
        return resultado;
    }
    
  //***************Funciones adicionales****************************
    public ArrayList<Publicacion> lstPublicacionXActividad(int idActividad, boolean todas){
        ArrayList<Publicacion> resultado=new ArrayList<Publicacion>();
        //---------------------------
        String consulta="";
         PreparedStatement stmt=null;
        try {
            if(todas){
                 consulta="SELECT * FROM Publicacion WHERE idActividad=?;";
                 stmt=this.obtenerConexion(consulta);
                 stmt.setInt(1, idActividad);
            }
            else{
                consulta="SELECT * FROM Publicacion WHERE idActividad=? and habilitada=?;";
                stmt=this.obtenerConexion(consulta);
                stmt.setInt(1, idActividad);
                stmt.setBoolean(2, true);
            }

            
            ResultSet datos=stmt.executeQuery();
            while(datos.next()){
                resultado.add(new Publicacion(datos.getInt(1), datos.getString(2), datos.getInt(3), datos.getBoolean(4)));
            }
            datos.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cerrarConexion();
        //---------------------------
        return resultado;
    }
    public ArrayList<Publicacion> lstCompletoPublicacionXActividad(int idActividad){
        ArrayList<Publicacion> resultado=new ArrayList<Publicacion>();
        //---------------------------
        String consula="SELECT * FROM Publicacion WHERE idActividad=?;";
        PreparedStatement stmt=this.obtenerConexion(consula);
        try {
            stmt.setInt(1, idActividad);
            
            ResultSet datos=stmt.executeQuery();
            while(datos.next()){
                resultado.add(new Publicacion(datos.getInt(1), datos.getString(2), datos.getInt(3), datos.getBoolean(4)));
            }
            datos.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cerrarConexion();
        //---------------------------
        return resultado;
    }
    public Publicacion obtenerPublicacion(int idPublicacion){
        Publicacion resultado=null;
        //--------------------
        String consulta="SELECT * FROM Publicacion WHERE id=?;";
        PreparedStatement stmt=this.obtenerConexion(consulta);
        try {
            stmt.setInt(1, idPublicacion);
            ResultSet datos=stmt.executeQuery();
            datos.next();
            resultado=new Publicacion(datos.getInt(1),datos.getString(2),datos.getInt(3),datos.getBoolean(4));
            datos.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cerrarConexion();
        
        //---------------
        return resultado;
    }
    public Actividad obtenerActividad(int idActividad){
        Actividad resultado=null;
        //------------------------------
        String consulta="SELECT * FROM Actividad WHERE id=?;";
        PreparedStatement stmt=this.obtenerConexion(consulta);
        
        try {
            stmt.setInt(1, idActividad);
            ResultSet datos=stmt.executeQuery();
            datos.next();
            
            resultado=new Actividad(datos.getInt(1),datos.getString(2));
            datos.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cerrarConexion();
        //---------------------------------
        return resultado;
    }
    public Socio obtenerSocio(int idSocio){
        Socio resultado=null;
        //-----------------------
         String consulta="SELECT * FROM Socio WHERE id=?;";
        PreparedStatement stmt=this.obtenerConexion(consulta);
        try {
            stmt.setInt(1, idSocio);
            ResultSet datos=stmt.executeQuery();
            datos.next();
            resultado= new Socio(datos.getInt(1), datos.getString(2), datos.getString(3), datos.getString(4), datos.getString(5), datos.getString(6), datos.getString(7), datos.getInt(8));
            datos.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(GestorDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        //-----------------------
        return resultado;
    }
}//finclase
