<%-- 
    Document   : Login
    Created on : 11/11/2018, 21:24:57
    Author     : roberto
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="css/miEstilo.css" type="text/css"/>
        <script src="Script/LoginValidacion.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-dark bg-dark">
            <form class="form-inline">
                <a href="index.html">
                    <button class="btn btn-outline-success text-white" type="button">
                        Home
                    </button>
                </a>
                <a href="NoticiasController">
                    <button class="btn btn-outline-success text-white" type="button">
                        Noticias
                    </button>
                </a>
                <a href="CuotasClub" >
                    <button class="btn btn-outline-success text-white" type="button">
                        Cuotas
                    </button>
                </a>
                <a href="Login" >
                    <button class="btn btn-outline-success text-white" type="button">
                     Administradores
                    </button>
                </a>
                
            </form>                  
        </nav>
        
        <h1 class="titulo">Login</h1>
        
        <form action="Login" method="POST" onsubmit="return validar()">
            <div class="formulario">
                <div>
                    <input type="text" name="txtUsuario" id="txtUsuario" placeholder="Usuario"/>
                </div>
                <div>
                    <input type="password" name="txtPassword" id="txtPassword" placeholder="Password"/>
                </div>
                <input type="submit" value="Login" class="btn btn-success"/>
            </div>
        </form>
        
        <p id="mensajeScript" class="mensajeError"></p>
        
        <div>
            <c:if test="${estado ==2}">
                <p class="mensajeError">Usuario o Password invalidos</p>
            </c:if>
        </div>
      
    </body>
</html>
