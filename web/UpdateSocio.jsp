<%-- 
    Document   : UpdateSocio
    Created on : 10/11/2018, 18:33:41
    Author     : roberto
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="Modelo.Usuario" %>
<jsp:useBean class="Modelo.Usuario"
             id="usuario"
             scope="session"
             >
 </jsp:useBean>
<c:if test="${!usuario.estado}">
    <% response.sendRedirect("Login");%>
</c:if>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Socio</title>
        <script src="Script/UpdateSocioValidacion.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="css/miEstilo.css" type="text/css"/>
    </head>
    <body>
        <nav class="navbar navbar-dark bg-dark">
            <form class="form-inline">
              
                <a href="PublicacionesController">
                    <button class="btn btn-outline-success text-white" type="button">
                        Publicaciones
                    </button>
                </a>
                
                <a href="ActividadesController">
                    <button class="btn btn-outline-success text-white" type="button">
                        Actividades
                    </button>
                </a>
                <a href="SociosController">
                    <button class="btn btn-outline-success text-white" type="button">
                        Socios
                    </button>
                </a>
                <a href="TipoCuotaController">
                    <button class="btn btn-outline-success text-white" type="button">
                     Tipos de cuota
                    </button>
                </a>
               <a href="Reportes">
                    <button class="btn btn-outline-success text-white" type="button">
                     Reportes
                    </button>
                </a>
                <p class="mensajeTitulo">Hola ${usuario.nombreUsuario}</p>
                <a href="LoginOut" class="loginOut">
                        <button type="button" class="btn btn-danger">
                            Login Out
                        </button>
                </a>
            </form>                  
        </nav>
        
        
                <h1 class="titulo">Datos personales</h1>
                
                <form action="ConsultarSocio" method="POST" onsubmit="return validar()" class="formulario">
            <div>
                <div>
                    <p><b>ID:</b>${socio.getId()}</p>
                     <input type="hidden" value="${socio.getId()}" name="txtIdSocio"/>
                </div>
                <div>
                    <label for="txtApellido"><b>Apellido:</b></label>
                    <input type="text" name="txtApellido" id="txtApellido" value="${socio.getApellido()}"/>
                </div>
                <div>
                    <label for="txtNombre"><b>Nombre:</b></label>
                    <input type="text" name="txtNombre" id="txtNombre" value="${socio.getNombre()}"/>
                </div>
                <div>
                    <label for="txtDNI"><b>DNI:</b></label>
                    <input type="text" name="txtDNI" id="txtDNI" value="${socio.getDni()}"/>
                </div>
                <div>
                    <label for="txtDireccion"><b>Dirección:</b></label>
                    <input type="text" name="txtDireccion" id="txtDireccion" value="${socio.getDireccion()}"/>
                </div>
                <div>
                    <label for="txtTelefono"><b>Telefono:</b></label>
                    <input type="text" name="txtTelefono" id="txtTelefono" value="${socio.getTelefono()}"/>
                </div>
                <div>
                    <label for="txtMail"><b>Email:</b></label>
                    <input type="text" name="txtMail" id="txtMail" value="${socio.getMail()}"/>
                </div>
                <div>
                    <label for="txtTipoCuota"><b>Cuota:</b></label>
            
                        <select name="txtTipoCuota" size="0">
                            <c:forEach items="${lstTipoCuota}" var="r">
                                <c:if test="${r.getId() == socio.getIdTipoCuota()}">
                                    <option value="${r.getId()}" selected> ${r.getNombre()}</option>
                                </c:if>
                                <c:if test="${r.getId() != socio.getIdTipoCuota()}">
                                    <option value="${r.getId()}"> ${r.getNombre()}</option>
                                </c:if>
                            </c:forEach>
                        </select>
                    
                </div>
            </div>
                <input type="submit" value="Guardar Cambios" class="btn btn-success"/>
                
        </form>
                <p id="mensajeScript" class="mensajeError"></p>
                 <c:if test="${estado==1}">
                <p class="cargaCorrecta">Se modifico correctamente</p>
            </c:if>
             <c:if test="${estado==2}">
                <p class="cargaIncorrecta">No se pudo modificar</p>
            </c:if>
                
              
    </body>
</html>
