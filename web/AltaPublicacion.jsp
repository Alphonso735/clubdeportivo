<%-- 
    Document   : AltaPublicacion
    Created on : 06/11/2018, 11:07:18
    Author     : roberto
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="Modelo.Usuario" %>
<jsp:useBean class="Modelo.Usuario"
             id="usuario"
             scope="session"
             >
 </jsp:useBean>
<c:if test="${!usuario.estado}">
    <% response.sendRedirect("Login");%>
</c:if>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Alta de publicaciones</title>
        <title>Publicaciones</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="css/miEstilo.css" type="text/css"/>
        <script src="Script/AltaPublicacionValidacion.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-dark bg-dark">
            <form class="form-inline">
              
                <a href="PublicacionesController">
                    <button class="btn btn-outline-success text-white" type="button">
                        Publicaciones
                    </button>
                </a>
                
                <a href="ActividadesController">
                    <button class="btn btn-outline-success text-white" type="button">
                        Actividades
                    </button>
                </a>
                <a href="SociosController">
                    <button class="btn btn-outline-success text-white" type="button">
                        Socios
                    </button>
                </a>
                <a href="TipoCuotaController">
                    <button class="btn btn-outline-success text-white" type="button">
                     Tipos de cuota
                    </button>
                </a>
               <a href="Reportes">
                    <button class="btn btn-outline-success text-white" type="button">
                     Reportes
                    </button>
                </a>
                <p class="mensajeTitulo">Hola ${usuario.nombreUsuario}</p>
                <a href="LoginOut" class="loginOut">
                        <button type="button" class="btn btn-danger">
                            Login Out
                        </button>
                </a>
            </form>                  
        </nav>
           
                <h1 class="titulo">Nueva publicación</h1>
                
                <form action="CargaPublicaciones" method="POST" onsubmit="return validar()" class="formulario">
                    <div>
                        <label for="txtActividad">Actividad</label>
                        <select name="txtActividad" size="0">
                        <c:forEach items="${lstActividades}" var="r">
                               <option value="${r.getId()}">${r.getDescripcion()}</option>
                        </c:forEach>
                        </select>
                    </div>
                    <div>
                        <label for="txtPublicacion">Texto de la publicación</label>
                        <textarea name="txtPublicacion" id="txtPublicacion" cols="30" rows="3"></textarea> 
                    </div>
                    <div>
                        <input type="submit" value="Cargar" class="btn btn-success"/>
                    </div>
                </form>
                
                <p id="mensajeScript" class="mensajeError"></p>
        <c:if test="${estado==1}">
            <p class="cargaCorrecta">Se cargo correctamente</p>
        </c:if>
        <c:if test="${estado==2}">
            <p class="cargaIncorrecta">No se cargo la publicacion</p>
        </c:if>
            
          
    </body>
</html>



