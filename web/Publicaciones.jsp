<%-- 
    Document   : Publicaciones
    Created on : 02/11/2018, 11:23:24
    Author     : roberto
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="Modelo.Usuario" %>
<jsp:useBean class="Modelo.Usuario"
             id="usuario"
             scope="session"
             >
 </jsp:useBean>
<c:if test="${!usuario.estado}">
    <% response.sendRedirect("Login");%>
</c:if>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Publicaciones</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="css/miEstilo.css" type="text/css"/>
    </head>
    <body>
           <nav class="navbar navbar-dark bg-dark">
            <form class="form-inline">
              
                <a href="PublicacionesController">
                    <button class="btn btn-outline-success text-white" type="button">
                        Publicaciones
                    </button>
                </a>
                
                <a href="ActividadesController">
                    <button class="btn btn-outline-success text-white" type="button">
                        Actividades
                    </button>
                </a>
                <a href="SociosController">
                    <button class="btn btn-outline-success text-white" type="button">
                        Socios
                    </button>
                </a>
                <a href="TipoCuotaController">
                    <button class="btn btn-outline-success text-white" type="button">
                     Tipos de cuota
                    </button>
                </a>
               <a href="Reportes">
                    <button class="btn btn-outline-success text-white" type="button">
                     Reportes
                    </button>
                </a>
                <p class="mensajeTitulo">Hola ${usuario.nombreUsuario}</p>
                <a href="LoginOut" class="loginOut">
                        <button type="button" class="btn btn-danger">
                            Login Out
                        </button>
                </a>
            </form>                  
        </nav>
                
                <h1 class="titulo">Publicaciones</h1>
                <div class="MenuOpciones">
                    <a href="CargaPublicaciones">
                        <button type="button" class="btn btn-success">
                            Nueva publicación
                        </button>
                    </a>                   
                </div>        
        
        
        <form action="PublicacionesController" method="POST" class="formulario">
            
            <select name="txtActividad" size="0">
                <c:forEach items="${lstActividades}" var="r">
                    <option value="${r.getId()}">${r.getDescripcion()}</option>
                </c:forEach>
            </select>
          
            
            <input type="submit" value="Mostrar" class="btn btn-success"/>
        </form>
        
              
        <c:if test="${!lstPublicaciones.isEmpty()}">
          
            <div class="contenedor">
                <div class="contenedorTitulo">
                  
                        Publicaciones
                   
                </div>
                <div class="contenedorCuerpo">
                    <c:forEach items="${lstPublicaciones}" var="r">
                        <div class="contenedorCuerpoFila">
                            <div>
                                    <p>${r.getTexto()}</p>
                            </div>
                            <div>
                                <c:if test="${r.isHabilitada()}">
                                    <a href="/TPAbraham/EstadoPublicacion?idPublicacion=${r.getId()}">
                                        <button type="button" class="btn btn-danger">
                                        Desabilitar                                            
                                        </button>
                                    </a>
                                </c:if>
                                <c:if test="${!r.isHabilitada()}">
                                     <a href="/TPAbraham/EstadoPublicacion?idPublicacion=${r.getId()}">
                                         <button type="button" class="btn btn-primary">
                                         Habilitar                                             
                                         </button>
                                     </a>
                                </c:if>
                            </div>
                                
                            <div>
                               <a href="ModificarPublicacion?idPublicacion=${r.getId()}">
                                   <button type="button" class="btn btn-dark text-white">
                                       Modificar
                                   </button> 
                               </a>
                            </div>
                             <div>
                                 <a href="BorrarPublicacion?idPublicacion=${r.getId()}">
                                     <button type="button" class="btn btn-dark text-white">
                                     Borrar
                                     </button> 
                                 </a>
                             </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </c:if>
        
      
    </body>
</html>
