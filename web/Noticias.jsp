<%-- 
    Document   : Noticias
    Created on : 02/11/2018, 08:25:49
    Author     : roberto
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Noticias</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="css/miEstilo.css" type="text/css"/>
    </head>
    
    <nav class="navbar navbar-dark bg-dark">
            <form class="form-inline">
                <a href="index.html">
                    <button class="btn btn-outline-success text-white" type="button">
                        Home
                    </button>
                </a>
                <a href="NoticiasController">
                    <button class="btn btn-outline-success text-white" type="button">
                        Noticias
                    </button>
                </a>
                <a href="CuotasClub" >
                    <button class="btn btn-outline-success text-white" type="button">
                        Cuotas
                    </button>
                </a>
                <a href="Login" >
                    <button class="btn btn-outline-success text-white" type="button">
                     Administradores
                    </button>
                </a>
                
            </form>                  
        </nav>
    
    <h1 class="titulo">Noticias</h1>
        
    <form action="NoticiasController" method="POST" class="formulario">
            <div class="form-group">
                <select name="txtActividad" size="0">
                    <c:forEach items="${lstActividades}" var="r">
                        <option value="${r.getId()}">${r.getDescripcion()}</option>
                    </c:forEach>
                </select>
                <input type="submit" value="Mostrar" class="btn btn-success"/>
            </div>
        </form>
        
              
        <c:if test="${!lstPublicaciones.isEmpty()}">
            <div class="contenedor">
                <div class="contenedorTitulo">
                   
                        Publicaciones
                   
                </div>
                <div class="contenedorCuerpo">
                    <c:forEach items="${lstPublicaciones}" var="r">
                        <div class="contenedorCuerpoFila">
                            <p>${r.getTexto()}</p>
                        </div>
                    </c:forEach>
                </div>
            </div>
            
        </c:if>
        
       
    
</html>
