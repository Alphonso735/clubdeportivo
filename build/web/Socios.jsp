<%-- 
    Document   : Socios
    Created on : 10/11/2018, 17:20:14
    Author     : roberto
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="Modelo.Usuario" %>
<jsp:useBean class="Modelo.Usuario"
             id="usuario"
             scope="session"
             >
 </jsp:useBean>
<c:if test="${!usuario.estado}">
    <% response.sendRedirect("Login");%>
</c:if>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Socios</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="css/miEstilo.css" type="text/css"/>
    </head>
    <body>
        <nav class="navbar navbar-dark bg-dark">
            <form class="form-inline">
              
                <a href="PublicacionesController">
                    <button class="btn btn-outline-success text-white" type="button">
                        Publicaciones
                    </button>
                </a>
                
                <a href="ActividadesController">
                    <button class="btn btn-outline-success text-white" type="button">
                        Actividades
                    </button>
                </a>
                <a href="SociosController">
                    <button class="btn btn-outline-success text-white" type="button">
                        Socios
                    </button>
                </a>
                <a href="TipoCuotaController">
                    <button class="btn btn-outline-success text-white" type="button">
                     Tipos de cuota
                    </button>
                </a>
               <a href="Reportes">
                    <button class="btn btn-outline-success text-white" type="button">
                     Reportes
                    </button>
                </a>
                <p class="mensajeTitulo">Hola ${usuario.nombreUsuario}</p>
                <a href="LoginOut" class="loginOut">
                        <button type="button" class="btn btn-danger">
                            Login Out
                        </button>
                </a>
            </form>                  
        </nav>
        
                <h1 class="titulo">Socios</h1>
                
            
        <div class="MenuOpciones">
                    <a href="CargarSocio">
                        <button type="button" class="btn btn-success">
                           Nuevo Socio
                        </button>
                    </a>
                    
        </div>        
        
            
       
          
        <div class="contenedor">
            <div class="contenedorTitulo">
                Listado de socios
            </div>
            <div class="contenedorCuerpo">
                <c:forEach items="${lstSocios}" var="r">
                    <div class="contenedorCuerpoFila">
                        <div>
                            <p>${r.getApellido()},${r.getNombre()}</p>
                        </div>
                        <div>
                            <a href="ConsultarSocio?idSocio=${r.getId()}">
                                <button type="button" class="btn btn-dark">
                                    Datos personales
                                </button>
                            </a>
                        </div>
                        <div>
                           <a href="BorrarSocio?idSocio=${r.getId()}">
                                <button type="button" class="btn btn-dark">
                                    Borrar                                  
                                </button>
                           </a>
                        </div>
                        <div>
                            <a href="RegistrarPagoCuota?idSocio=${r.getId()}">
                                <button type="button" class="btn btn-dark">
                                    Registrar Pago
                                </button>
                            </a>
                        </div>
                        <div>
                            <a href="RegistrarActividad?idSocio=${r.getId()}">
                                <button type="button" class="btn btn-dark">
                                    Actividades
                                </button>
                            </a>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
      
    </body>
</html>
