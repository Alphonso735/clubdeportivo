<%-- 
    Document   : MontoAnual
    Created on : 11/11/2018, 20:38:27
    Author     : roberto
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="Modelo.Usuario" %>
<jsp:useBean class="Modelo.Usuario"
             id="usuario"
             scope="session"
             >
 </jsp:useBean>
<c:if test="${!usuario.estado}">
    <% response.sendRedirect("Login");%>
</c:if>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>MontoAnual</title>
        <script src="Script/MontoAnualValidacion.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="css/miEstilo.css" type="text/css"/>
    </head>
    <body>
       <nav class="navbar navbar-dark bg-dark">
            <form class="form-inline">
              
                <a href="PublicacionesController">
                    <button class="btn btn-outline-success text-white" type="button">
                        Publicaciones
                    </button>
                </a>
                
                <a href="ActividadesController">
                    <button class="btn btn-outline-success text-white" type="button">
                        Actividades
                    </button>
                </a>
                <a href="SociosController">
                    <button class="btn btn-outline-success text-white" type="button">
                        Socios
                    </button>
                </a>
                <a href="TipoCuotaController">
                    <button class="btn btn-outline-success text-white" type="button">
                     Tipos de cuota
                    </button>
                </a>
               <a href="Reportes">
                    <button class="btn btn-outline-success text-white" type="button">
                     Reportes
                    </button>
                </a>
                <p class="mensajeTitulo">Hola ${usuario.nombreUsuario}</p>
                <a href="LoginOut" class="loginOut">
                        <button type="button" class="btn btn-danger">
                            Login Out
                        </button>
                </a>
            </form>                  
        </nav>
        
                <h1 class="titulo">Monto Anual en cuotas</h1>
                
                <form action="MontoAnualCuotas" method="POST" onsubmit="return validar()" class="formulario">
                    <div>
                        <label for="txtAnio">Año</label>
                        <input type="number" min="2015" max="2018" name="txtAnio" id="txtAnio"/>
                    </div>

                    <input type="submit"   value="Consultar" class="btn btn-success"/>
               </form>
         <p id="mensajeScript" class="mensajeError"></p>
        <c:if test="${resultado != -1}">
           
                <p class="cargaCorrecta">El monto es de $ ${resultado}</p>
           
        </c:if>
        
     
    </body>
</html>
